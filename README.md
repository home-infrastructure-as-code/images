# images

Defines Dockerfiles for those services that do not have an official image.

Check https://gitlab.com/home-infrastructure-as-code/hiac for working examples.

