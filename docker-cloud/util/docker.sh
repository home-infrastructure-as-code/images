#!/bin/bash

set -o errexit

function check_docker_permissons {
	if ! id -nG "${USER}" | grep -qw "docker" ; then
		if [ "${EUID}" -ne 0 ] ; then
			echo "You must be member of the 'docker' group or root to run this command."
			exit 1
		fi
	fi
}
export check_docker_permissons

