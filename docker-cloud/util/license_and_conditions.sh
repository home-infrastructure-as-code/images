#!/bin/bash

set -o errexit

function print_warranty_and_conditions {
	cat <<- _TEXT_BLOCK
		GUARANTEE AND CONDITIONS
		================================================================================
		${HIAC_SCRIPT_NAME} v${HIAC_SCRIPT_VERSION} Copyright (C) 2021  Guillermo López Alejos
		This program comes with ABSOLUTELY NO WARRANTY; for details type
		"${HIAC_SCRIPT_NAME} warranty".
		This is free software, and you are welcome to redistribute it
		under certain conditions; type "${HIAC_SCRIPT_NAME} conditions" for details.
	_TEXT_BLOCK
}
export print_warranty_and_conditions

function print_warranty {
	cat <<- _TEXT_BLOCK
		WARRANTY
		================================================================================
		THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY
		APPLICABLE LAW.  EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT
		HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY
		OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO,
		THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
		PURPOSE.  THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM
		IS WITH YOU.  SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF
		ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
	_TEXT_BLOCK
}
export print_warranty

function print_conditions {
	cat <<- _TEXT_BLOCK
		CONDITIONS
		================================================================================
		View https://www.gnu.org/licenses/gpl-3.0.txt
	_TEXT_BLOCK
}
export print_conditions

