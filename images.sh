#!/bin/bash

#    Dockerized home infrastructure as code helper script.
#    Copyright (C) 2021  Guillermo López Alejos
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# For now only BASH is supported.
if [ "${BASH}x" = "x" ] ; then
	echo "Only BASH shell is supported." 1>&2
	exit 1
fi

set -o errexit
set -o nounset
set -o pipefail

IMAGES_SCRIPT_VERSION=0.0.1
IMAGES_SCRIPT_NAME="${0}"
IMAGES_SCRIPT_ABS="$(realpath "${IMAGES_SCRIPT_NAME}")"
IMAGES_SCRIPT_DIR_ABS="$(dirname "${IMAGES_SCRIPT_ABS}")"
IMAGES_CLOUD_DIR_ABS="${IMAGES_SCRIPT_DIR_ABS}/docker-cloud"
IMAGES_SERVICES_DIR_ABS="${IMAGES_CLOUD_DIR_ABS}/services"

# shellcheck source=docker-cloud/util/docker.sh
source "${IMAGES_SCRIPT_DIR_ABS}/docker-cloud/util/docker.sh"

# shellcheck source=docker-cloud/util/license_and_conditions.sh
source "${IMAGES_SCRIPT_DIR_ABS}/docker-cloud/util/license_and_conditions.sh"

###############################################################################
# 1. FUNCTIONS
###############################################################################

function print_help {
	cat <<- _TEXT_BLOCK
		HELP
		================================================================================
		<no help available>
	_TEXT_BLOCK
}

function print_usage_and_exit {
	cat 1>&2 <<- _TEXT_BLOCK
		${IMAGES_SCRIPT_NAME} v${IMAGES_SCRIPT_VERSION}

		Usage:

			${IMAGES_SCRIPT_NAME} <build | warranty | conditions>

	_TEXT_BLOCK

	exit 1
}

#
# Function: for_each_service
# 
# Iterates over all the services calling the given function on each.
#
# Parameters:
#
# ${1} - Function to invoke for each service. It is passed the following parameters: ${1}: the service identifier.
#
function for_each_service {
	service_handler="${1}"

	for service_abs in "${IMAGES_SERVICES_DIR_ABS}"/* ;
	do
		service_id="$(basename "${service_abs}")"
		"${service_handler}" "${service_id}"
	done
}

#
# Function: build_service_image
# 
# Builds the image for the given service.
#
# Parameters:
#
# ${1} - Service identifier.
#
function build_service_image {
	service_id="${1}"

	pushd "${IMAGES_SERVICES_DIR_ABS}/${service_id}"

	image_tag="$(cat image_tag)"

	docker build --tag "${image_tag}" "docker-build-ctx-src"

	popd
}

###############################################################################
# 2. INPUT PARAMETER VALIDATION
###############################################################################
COMMAND=${1:-}
if [ -z "${COMMAND}" ] ; then
	echo "You must specify a command." 1>&2
	print_usage_and_exit
fi
shift

for param in "${@}" ; do
case "${param}" in
	*)
		echo "Unknown parameter ${param}." 1>&2
		print_help
		exit 1
	;;
esac
done

###############################################################################
## 4. COMMAND
###############################################################################

exit_code=0

case ${COMMAND} in
"build")
	check_docker_permissons

	for_each_service build_service_image

	;;

"warranty")
	print_warranty
	;;

"conditions")
	print_conditions
	;;

*)
	echo "Unsupported command '${COMMAND}'." 1>&2
	print_usage_and_exit
	;;
esac

exit "${exit_code}"

